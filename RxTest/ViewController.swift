//
//  ViewController.swift
//  RxTest
//
//  Created by rogerio on 04/12/2018.
//  Copyright © 2018 rogerio. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
    var dispose:DisposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let server:Server = Server()
       server.getData()
        .observeOn(MainScheduler.instance)
        .subscribe(
            onNext: { data in print("Data = ",data) },
            onError: { error in print("Error =", error) },
            onCompleted: { print("Complete") }
        ).disposed(by: dispose)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

