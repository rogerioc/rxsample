//
//  Server.swift
//  RxTest
//
//  Created by rogerio on 04/12/2018.
//  Copyright © 2018 rogerio. All rights reserved.
//

import Foundation
import RxAlamofire
import RxSwift
struct User: Codable{
    var name:String
    var email:String
    //    init(name: String, email: String) {
    //
    //    }
}

class Server {
    func getData() -> Observable<User> {
       return requestData(.get,"https://www.mocky.io/v2/5c06fddc3000005500d25964")
            .map({ (data) -> User in
                print("Return = ",data)
                let responseTuple = data as? (HTTPURLResponse, Data)
                
                guard let jsonData = responseTuple?.1 else {
                    throw NSError(
                        domain: "",
                        code: -1,
                        userInfo: [NSLocalizedDescriptionKey: "Could not decode object"]
                    )
                }
                let jsonDecoder = JSONDecoder()
                let user = try jsonDecoder.decode(User.self, from: jsonData)
                return user
            })
    }
}
