//
//  RxTestTests.swift
//  RxTestTests
//
//  Created by rogerio on 04/12/2018.
//  Copyright © 2018 rogerio. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
import OHHTTPStubs

@testable import RxTest

class RxTestTests: XCTestCase {
    
    var server: Server!
    let dispose = DisposeBag()
    
    override func setUp() {
        super.setUp()
        server = Server()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testRX() {
        let expectation = self.expectation(description: "calls the callback with a resource object")
        let stubbedJSON = [
            "name": "Name",
            "email" : "Email"
            ]
        
        stub(condition: isMethodGET()) { _ in
            return OHHTTPStubsResponse(
                jsonObject: stubbedJSON,
                statusCode: 200,
                headers: [ "Content-Type": "application/json" ]
            )
        }
        server.getData()
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { data in
                    print("Data = ",data)
                    XCTAssertEqual(data.name, "Name")
                    expectation.fulfill()
                },
                onError: { error in print("Error =", error) },
                onCompleted: {
                    print("Complete")
                    
                }
            ).disposed(by: dispose)
        self.waitForExpectations(timeout: 0.3, handler: .none)
    }
    
    
   
    
}
